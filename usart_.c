#include "stm32f10x.h"

/**
	* @brief  Инициализация USART1
  * @param  None
  * @retval None
  */
void usart_init(void)
{
	RCC -> APB2ENR |= RCC_APB2ENR_USART1EN; //Включаем тактирование USART1
	RCC -> APB2ENR |= RCC_APB2ENR_IOPBEN; //Включаем тактирование порта B
	RCC -> APB2ENR |= RCC_APB2ENR_AFIOEN; //Включаем альтернативные функции ввода вывода
	
	AFIO -> MAPR |= AFIO_MAPR_USART1_REMAP; //Переключаем маппирование USART1 на TX/PB6, RX/PB7
	GPIOB -> CRL |= (1 << 24 | 1 << 25 | 1 << 28 | 1 << 29); // PB6 PB7 режим вывода 50 МГц
	GPIOB -> CRL |= (1 << 27 | 1 << 31); //PB6 PB7 альтернативный режим output push-pull
	
	//Настройка USART1
	USART1 -> CR1 |= USART_CR1_UE; //Включаем USART
	USART1 -> CR1 |= USART_CR1_TE; //Включаем передатчик
	
	//USART1 -> BRR |= 0x753; //baudrate 38400
	USART1 -> BRR |= 0x139; //baudrate 230400
	
}

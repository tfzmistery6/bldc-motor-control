/******************************************************************************
 * @file    main.c
 * @brief   Главный файл системы управления bldc мотора
 * @version v0.1
 * @date    09.03.2021
 * @author  Власовский Алексей Игоревич & Мануйлов Александр Владимирович
 * @note    АО "ОКБ МЭЛ" г.Калуга
 ******************************************************************************/

#include "stm32f10x.h"
#include "adc_.h"
#include "var_.h"
#include "SysTick_.h"
#include "usart_.h"
#include "TIM1_.h"
#include "gpio_.h"

/*____________________PROTOTYPES____________________*/
void USART1_TXBuf_append (char *buffer); //Отправка данных в USART1
static char *utoa_cycle_sub(uint32_t value, char *buffer); //Преобразование uint to ascii

/*______________________MAIN________________________*/

int main()
{
	SystemInit();
	ADC_Init();
	SysTick_init();
	//usart_init();
	TIM1_init();
	GPIO_init();
	
	SysTick_counter = 0;
	SysTick_counter_2 = 0;
	speed = 0;
	value_buf = 0;
	data_rdy = 0;
	
	TX_BUF_empty_space = TX_BUF_SIZE; //Колчиство свободных байт в TX_BUF
	TXi_w = 0; //Указатель куда писать байт в массив
	TXi_t = 0; //Указатель откуда отправлять байт в массив
	
	HS1_buffer = 0;
	HS2_buffer = 0;
	HS3_buffer = 0;
	
	k = 0.1;
	
	NVIC_EnableIRQ(USART1_IRQn);
	
	while(1);
}

/*_______________INTERRUPTS HANDLERS_______________*/

//Прерывание SysTick 32 000 Гц
//Расчёт частоты: 72 000 000 / (SysTick->LOAD + 1) = частота в Гц
//В нашем случае 72 000 000 / (0x8C9 + 1) =  32 000 Гц
void SysTick_Handler (void)
{
	ADC0_result = kalman_filter(ADC0_result, (ADC1 -> DR)/8, k);
	//ADC4_result = (ADC1 ->JDR2);
	//ADC5_result = (ADC1 ->JDR3);
	//ADC6_result = (ADC1 ->JDR4);
	
	if (ADC0_result > 512){ADC0_result = 512;}
	
	//фильтруем датчики холла через переменную
	HS1_buffer = (HS1_buffer << 1) + ((GPIOC -> IDR & GPIO_ODR_ODR1) >> 1);
	HS2_buffer = (HS2_buffer << 1) + ((GPIOC -> IDR & GPIO_ODR_ODR2) >> 2);
	HS3_buffer = (HS3_buffer << 1) + ((GPIOC -> IDR & GPIO_ODR_ODR3) >> 3);
	
//	SysTick_counter++;
//	if (SysTick_counter > 16000)
//	{
//		SysTick_counter = 16000;
//	}
//	
//	SysTick_counter_2++;
	
//	//Если ловим фронт первого датчика
//	if (HS1_buffer == 0x7F)
//	{
//		speed = SysTick_counter;
//		//USART1_TXBuf_append(utoa_cycle_sub(SysTick_counter, buffer_));
//		//USART1_TXBuf_append("\n");
//		SysTick_counter = 0;
//	}
	
//	if (SysTick_counter_2 >= 32)
//	{
//		value_buf = (int) speed;
//		USART1_TXBuf_append(utoa_cycle_sub(value_buf, buffer_));
//		USART1_TXBuf_append(" ");
//		USART1_TXBuf_append(utoa_cycle_sub(ADC4_result, buffer_));
//		USART1_TXBuf_append(" ");
//		USART1_TXBuf_append(utoa_cycle_sub(ADC5_result, buffer_));
//		USART1_TXBuf_append(" ");
//		USART1_TXBuf_append(utoa_cycle_sub(ADC6_result, buffer_));
//		USART1_TXBuf_append("\n");
//		SysTick_counter_2 = 0;
//	}
	
	//Если ручка слегка повёрнута
	if ( ADC0_result >= 20 )
	{
		if((HS1_buffer == 0xFF) && (HS2_buffer == 0) && (HS3_buffer == 0))
		{
			TIM1 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE; //канал 1 шим
			TIM1 -> CCER &= ~(TIM_CCER_CC2E | TIM_CCER_CC2NE); //канал 2 выкл
			TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3 земля
			TIM1 -> CCR1 = ARELOAD_VAL - ADC0_result;
			TIM1 -> CCR2 = 0;
			TIM1 -> CCR3 = ARELOAD_VAL;
		}
		
		if((HS1_buffer == 0xFF) && (HS2_buffer == 0xFF) && (HS3_buffer == 0))
		{
			TIM1 -> CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC1NE); //канал 1 выкл
			TIM1 -> CCER |= TIM_CCER_CC2E | TIM_CCER_CC2NE; //канал 2 вкл шим
			TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3 вкл земля
			TIM1 -> CCR1 = 0;
			TIM1 -> CCR2 = ARELOAD_VAL - ADC0_result;
			TIM1 -> CCR3 = ARELOAD_VAL;
		}
		
		if((HS1_buffer == 0) && (HS2_buffer == 0xFF) && (HS3_buffer == 0))
		{
			TIM1 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE; //канал 1 земля
			TIM1 -> CCER |= TIM_CCER_CC2E | TIM_CCER_CC2NE; //канал 2 вкл шим
			TIM1 -> CCER &= ~(TIM_CCER_CC3E | TIM_CCER_CC3NE); //канал 3 выкл
			TIM1 -> CCR1 = ARELOAD_VAL;
			TIM1 -> CCR2 = ARELOAD_VAL - ADC0_result;
			TIM1 -> CCR3 = 0;
		}
		
		if((HS1_buffer == 0) && (HS2_buffer == 0xFF) && (HS3_buffer == 0xFF))
		{
			TIM1 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE; //канал 1 земля
			TIM1 -> CCER &= ~(TIM_CCER_CC2E | TIM_CCER_CC2NE); //канал 2 выкл
			TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3 шим
			TIM1 -> CCR1 = ARELOAD_VAL;
			TIM1 -> CCR2 = 0;
			TIM1 -> CCR3 = ARELOAD_VAL - ADC0_result;
		}
		
		if((HS1_buffer == 0) && (HS2_buffer == 0) && (HS3_buffer == 0xFF))
		{
			TIM1 -> CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC1NE); //канал 1 выкл
			TIM1 -> CCER |= TIM_CCER_CC2E | TIM_CCER_CC2NE; //канал 2 земля
			TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3 шим
			TIM1 -> CCR1 = 0;
			TIM1 -> CCR2 = ARELOAD_VAL;
			TIM1 -> CCR3 = ARELOAD_VAL - ADC0_result;
		}
		
		if((HS1_buffer == 0xFF) && (HS2_buffer == 0) && (HS3_buffer == 0xFF))
		{
			TIM1 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE; //канал 1 шим
			TIM1 -> CCER |= TIM_CCER_CC2E | TIM_CCER_CC2NE; //канал 2 земля
			TIM1 -> CCER &= ~(TIM_CCER_CC3E | TIM_CCER_CC3NE); //канал 3 выкл
			TIM1 -> CCR1 = ARELOAD_VAL - ADC0_result;
			TIM1 -> CCR2 = ARELOAD_VAL;
			TIM1 -> CCR3 = 0;
		}
	}
	else
	{
			TIM1 -> CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC1NE); //канал 1 выкл
			TIM1 -> CCER &= ~(TIM_CCER_CC2E | TIM_CCER_CC2NE); //канал 2 выкл
			TIM1 -> CCER &= ~(TIM_CCER_CC3E | TIM_CCER_CC3NE); //канал 3 выкл
			TIM1 -> CCR1 = 0;
			TIM1 -> CCR2 = 0;
			TIM1 -> CCR3 = 0;
	}
}


//Прерывание USART1
void USART1_IRQHandler(void)
{
	if (USART1 -> SR & USART_CR1_TXEIE) //data is transferred to the shift register
	{
		if (TXi_t != TXi_w)
		{
			USART1 -> DR = TX_BUF[TXi_t];
			TXi_t ++;
			TX_BUF_empty_space++;
	
			if (TXi_t >= 256){ TXi_t = 0; }
		}
		else
		{
			USART1 -> CR1 &= ~USART_CR1_TXEIE; //Запретить прерывания на передачу
			
		}
	}
}

//Указание точки входа (костыль по факту)
void Reset_Handler (void)
{
	main();
}

/*____________________FUNCTIONS____________________*/

/**
	* @brief  Перевод числа в аски символы методом циклического вычитания
	* @param  value - число, которое необходимо преобразовать
	*					*buffer - адрес массив, куда писать результат
  * @retval Возвращает буфер с записанным результататом преобразования
  */
static char *utoa_cycle_sub(uint32_t value, char *buffer)
{
	if(value == 0)
	{
		buffer[0] = '0';
		buffer[1] = 0;
		return buffer;
	}
	char *ptr = buffer;
	uint8_t i = 0;
	do
	{
		uint32_t pow10 = pow10Table32[i++];
		uint8_t count = 0;
		while(value >= pow10)
		{
			 count ++;
			 value -= pow10;
		}
		*ptr++ = count + '0';
	}while(i < 10);
	*ptr = 0;
	// удаляем ведущие нули
	while(*buffer == '0')
	{
	++buffer;
	}
	return buffer;
}

/**
	* @brief  Запись данных в циклический буфер на отправку через USART
	* @param  Массив данных, предназначенных на отправку
  * @retval None
  */
void USART1_TXBuf_append (char *buffer)
{
	//Пока в буфере есть данные
	while (*buffer)
	{
		TX_BUF[TXi_w] = *buffer++;
		TXi_w++;
		TX_BUF_empty_space--;
		
		if (TXi_w >= 256){ TXi_w = 0; }
	}
	
	USART1 -> CR1 |= USART_CR1_TXEIE; //разрешить прерывание на передачу
	
	//блок ниже вроде как не нужен, потому что флаг TXE не может быть сброшен никогда
	/*
	//Если USART ничего не отправляет, то необходимо отправить один бит
	if (USART1 -> ISR & (1 << 7)) //data is transferred to the shift register
	{
		USART1 -> TDR = TX_BUF[TXi_t];
		TXi_t ++;
		TX_BUF_empty_space++;
	}
	*/
	
	if (TXi_t >= 256){ TXi_t = 0; }
}

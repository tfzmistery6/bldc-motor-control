#include <stm32f10x.h>

/* PROTOTYPES */
void ADC_Init (void);
int kalman_filter(uint16_t y, uint16_t m, float k);

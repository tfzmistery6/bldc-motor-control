#include "stm32f10x.h"

uint16_t ADC0_result;
uint16_t ADC4_result;
uint16_t ADC5_result;
uint16_t ADC6_result;

uint16_t SysTick_counter = 0;
uint16_t SysTick_counter_2 = 0;
uint16_t speed = 0;
_Bool data_rdy = 0;
uint16_t speed_buf = 0;

uint8_t HS1_buffer;
uint8_t HS2_buffer;
uint8_t HS3_buffer;

_Bool HS1;
_Bool HS2;
_Bool HS3;

//Циклический буфер на отправку
#define TX_BUF_SIZE 256
int TX_BUF_empty_space = TX_BUF_SIZE; //Колчиство свободных байт в TX_BUF
int TXi_w = 0; //Указатель куда писать байт в массив
int TXi_t = 0; //Указатель откуда отправлять байт в массив
char TX_BUF[TX_BUF_SIZE] = {'\0'}; //Циклический массив для отправки данных по USART

//Таблица степеней десятки
const uint32_t  pow10Table32[]=
{
    1000000000ul,
    100000000ul,
    10000000ul,
    1000000ul,
    100000ul,
    10000ul,
    1000ul,
    100ul,
    10ul,
    1ul
};

char buffer_[10]; //Буфер для чисел, которые необходимо вывести в USART1
uint32_t value_buf = 0;

float k = 0.1; //коэффициент фильтрации для экспоненциального фильтра

#define ARELOAD_VAL 512

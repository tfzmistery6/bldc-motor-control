#include "stm32f10x.h"

/** 
 * @brief  SysTick initialization.
 * @param  none.
 * @return none.
 */
void SysTick_init(void)
{
	SysTick->CTRL = 0x00; //disable SysTick during setup
	//SysTick->LOAD = 0x2326; //RELOAD value //8 кГц
	//SysTick->LOAD = 0x1193; //RELOAD value //16 кГц
	SysTick->LOAD = 0x8C9; //RELOAD value //32 кГц
	//SysTick->LOAD = 0x464; //RELOAD value //64 кГц
	SysTick->VAL = 0x1C1F; //clear timer value
	SysTick->CTRL |= (1<<0 | 1<<2 | 1<<1); //freq from core`s freq,; irq enable
}

#include <stm32f10x.h>
#include <ADC_.h>

//Инициализаця АЦП
void ADC_Init (void) {
	
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;    // Разрешить тактирование порта PORTA
	 //Конфигурирование PORTA.0 - аналоговый вход
  GPIOA->CRL   &= ~GPIO_CRL_MODE0;       //Очистить биты MODE
  GPIOA->CRL   &= ~GPIO_CRL_CNF0;        //Очистить биты CNF
  //Конфигурирование PORTA.3 - аналоговый вход
  GPIOA->CRL   &= ~GPIO_CRL_MODE3;       //Очистить биты MODE
  GPIOA->CRL   &= ~GPIO_CRL_CNF3;        //Очистить биты CNF
  //Конфигурирование PORTA.4 - аналоговый вход
  GPIOA->CRL   &= ~GPIO_CRL_MODE4;       //Очистить биты MODE
  GPIOA->CRL   &= ~GPIO_CRL_CNF4;        //Очистить биты CNF
  //Конфигурирование PORTA.5 - аналоговый вход
  GPIOA->CRL   &= ~GPIO_CRL_MODE5;       //Очистить биты MODE
  GPIOA->CRL   &= ~GPIO_CRL_CNF5;        //Очистить биты CNF
  //Конфигурирование PORTA.6 - аналоговый вход
  GPIOA->CRL   &= ~GPIO_CRL_MODE6;       //Очистить биты MODE
  GPIOA->CRL   &= ~GPIO_CRL_CNF6;        //Очистить биты CNF
 
  RCC->APB2ENR |=  RCC_APB2ENR_ADC1EN;   //подаем тактирование АЦП 
  RCC->CFGR    &= ~RCC_CFGR_ADCPRE;      //входной делитель
  ADC1->CR1     =  0;                    //предочистка регистра 
  ADC1->CR2    |=  ADC_CR2_CAL;          //запуск калибровки 
  while (!(ADC1->CR2 & ADC_CR2_CAL)){};  //ждем окончания калибровки
  ADC1->CR2     =  ADC_CR2_JEXTSEL;      //выбрать источником запуска разряд  JSWSTART
  ADC1->CR2    |=  ADC_CR2_JEXTTRIG;     //разр. внешний запуск инжектированной группы
  ADC1->CR2    |=  ADC_CR2_CONT;         //режим непрерывного преобразования 
  ADC1->CR1    |=  ADC_CR1_SCAN;         //режим сканирования (т.е. несколько каналов)
  ADC1->CR1    |=  ADC_CR1_JAUTO;     	 //автомат. запуск инжектированной группы
  ADC1->JSQR    =  (uint32_t)(4-1)<<20;  //задаем количество каналов в инжектированной группе
  ADC1->JSQR   |=  (uint32_t)3<<(5*0);   //номер канала для первого преобразования             
  ADC1->JSQR   |=  (uint32_t)4<<(5*1);   //номер канала для второго преобразования
  ADC1->JSQR   |=  (uint32_t)5<<(5*2);   //номер канала для третьего преобразования
  ADC1->JSQR   |=  (uint32_t)6<<(5*3);   //номер канала для четвертого преобразования
  ADC1->CR2    |=  ADC_CR2_ADON;         //включить АЦП
  ADC1->CR2    |=  ADC_CR2_JSWSTART;     //запустить процес преобразования
}

/**
  * @brief  Фильтр Калмана для фильтрации помех
  * @param  y - предыдущее отфильтрованное измерение
  * 		m - текущее измерение
  * 		k - коэффициент фильтрации
  * @retval Возвращает отфильтрованное измерение
  */
int kalman_filter(uint16_t y, uint16_t m, float k)
{
	return (int) ((m*k)+((1-k)*y));
}

